﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NScripter_English_to_Japanese
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("NScripter English to Japanese");
            Console.WriteLine("By ShikiGami\n");

            Console.WriteLine("Reading files...\n");
            Variables.EnglishFile = File.ReadAllLines("result.txt", System.Text.Encoding.GetEncoding("shift_jis"));
            Variables.JapaneseFile = File.ReadAllLines("resultoriginal.txt", System.Text.Encoding.GetEncoding("shift_jis"));

            foreach (string en_line in Variables.EnglishFile)
            {
                //If we are in the text region of the file
                if (Variables.LinePosition > 49305 && Variables.LinePosition < 261720)
                {
                    //If the line appears to be an original Japanese Line commented
                    if ((en_line.StartsWith(";") && !(
                        en_line.StartsWith(";a") || en_line.StartsWith(";b") || en_line.StartsWith(";c") ||
                        en_line.StartsWith(";d") || en_line.StartsWith(";e") || en_line.StartsWith(";f") ||
                        en_line.StartsWith(";g") || en_line.StartsWith(";h") || en_line.StartsWith(";i") ||
                        en_line.StartsWith(";j") || en_line.StartsWith(";k") || en_line.StartsWith(";l") ||
                        en_line.StartsWith(";m") || en_line.StartsWith(";n") || en_line.StartsWith(";o") ||
                        en_line.StartsWith(";p") || en_line.StartsWith(";q") || en_line.StartsWith(";r") ||
                        en_line.StartsWith(";s") || en_line.StartsWith(";t") || en_line.StartsWith(";u") ||
                        en_line.StartsWith(";v") || en_line.StartsWith(";w") || en_line.StartsWith(";x") ||
                        en_line.StartsWith(";y") || en_line.StartsWith(";z") || en_line.StartsWith(";■") ||
                        en_line.StartsWith(";A") || en_line.StartsWith(";B") || en_line.StartsWith(";C") ||
                        en_line.StartsWith(";D") || en_line.StartsWith(";E") || en_line.StartsWith(";F") ||
                        en_line.StartsWith(";G") || en_line.StartsWith(";H") || en_line.StartsWith(";I") ||
                        en_line.StartsWith(";J") || en_line.StartsWith(";K") || en_line.StartsWith(";L") ||
                        en_line.StartsWith(";M") || en_line.StartsWith(";N") || en_line.StartsWith(";O") ||
                        en_line.StartsWith(";P") || en_line.StartsWith(";Q") || en_line.StartsWith(";R") ||
                        en_line.StartsWith(";S") || en_line.StartsWith(";T") || en_line.StartsWith(";U") ||
                        en_line.StartsWith(";V") || en_line.StartsWith(";W") || en_line.StartsWith(";X") ||
                        en_line.StartsWith(";Y") || en_line.StartsWith(";Z") || en_line.StartsWith(";	") ||
                        en_line.StartsWith("; a") || en_line.StartsWith("; b") || en_line.StartsWith("; c") ||
                        en_line.StartsWith("; d") || en_line.StartsWith("; e") || en_line.StartsWith("; f") ||
                        en_line.StartsWith("; g") || en_line.StartsWith("; h") || en_line.StartsWith("; i") ||
                        en_line.StartsWith("; j") || en_line.StartsWith("; k") || en_line.StartsWith("; l") ||
                        en_line.StartsWith("; m") || en_line.StartsWith("; n") || en_line.StartsWith("; o") ||
                        en_line.StartsWith("; p") || en_line.StartsWith("; q") || en_line.StartsWith("; r") ||
                        en_line.StartsWith("; s") || en_line.StartsWith("; t") || en_line.StartsWith("; u") ||
                        en_line.StartsWith("; v") || en_line.StartsWith("; w") || en_line.StartsWith("; x") ||
                        en_line.StartsWith("; y") || en_line.StartsWith("; z") || en_line.StartsWith(";  ") ||
                        en_line.StartsWith(";＜") || en_line.StartsWith(";▲") || en_line.StartsWith(";?") ||
                        en_line.StartsWith(";ψ") || en_line.StartsWith(";*") || en_line.StartsWith(";-") ||
                        en_line.StartsWith(";+") || en_line.StartsWith(";_") || en_line.StartsWith(";;") ||
                        en_line == ";" || en_line == ";!sd@" || (en_line.StartsWith(";!d") && en_line.EndsWith("0/")))))
                    {
                        //Uncommenting line for search in original file
                        var search_val = Variables.EnglishFile[Variables.LinePosition].Remove(0, 1);
                        //Was the line permanently uncommented?
                        var transformed = false;

                        //Search in the japanese file
                        foreach (string jp_line in Variables.JapaneseFile)
                        {
                            //If the uncomented line is found also uncomented in the original japanese
                            //file, then it is assumed that the line should be uncommented
                            if (jp_line.Equals(search_val))
                            {
                                Console.WriteLine("Line transformed at " + (Variables.LinePosition + 1).ToString());
                                Variables.EnglishFile[Variables.LinePosition] = search_val;
                                transformed = true;
                                break;
                            }
                        }

                        if (!transformed)
                        {
                            //Should the line be permenently uncommented?
                            var change = true;

                            //Search in the japanese file
                            foreach (string jp_line in Variables.JapaneseFile)
                            {
                                //If the commented line is found also commented in the original japanese
                                //file, then it is assumed that the line should not be uncommented
                                if (jp_line.Equals(Variables.EnglishFile[Variables.LinePosition]))
                                {
                                    change = false;
                                    break;
                                }
                            }
                            //If the commented line is not found in the original japanese file
                            //then it is assumed that the line should be uncommented
                            //(CAUTION: THIS ALSO UNCOMMENTS EXTRA ENGLISH COMMENTS)
                            if (change)
                            {
                                Console.WriteLine("Line transformed at " + (Variables.LinePosition + 1).ToString());
                                Variables.EnglishFile[Variables.LinePosition] = search_val;
                                transformed = true;
                            }
                        }

                        if (transformed)
                        {
                            //See if this is the start to a new Japanese Block
                            if (Variables.JapaneseBlockStart == 0)
                            {
                                Variables.JapaneseBlockStart = Variables.LinePosition;
                                Variables.JapaneseLineCounter = 0;
                                Variables.JapaneseLineIndex = new int[200];
                                Variables.JapaneseAtIndex = new int[200][];
                                Variables.JapaneseLineIndex[Variables.JapaneseLineCounter] = Variables.LinePosition;
                                Variables.JapaneseAtIndex[Variables.JapaneseLineCounter] = new int[200];
                            }
                            else
                            {
                                Variables.JapaneseLineCounter++;
                                Variables.JapaneseLineIndex[Variables.JapaneseLineCounter] = Variables.LinePosition;
                                Variables.JapaneseAtIndex[Variables.JapaneseLineCounter] = new int[200];
                            }

                            //Count the number of @ in the text
                            Variables.JapaneseATCount += en_line.Count(f => f == '@');
                            var at_index_next = 0;
                            for (int i = 1; i <= en_line.Count(f => f == '@'); i++)
                            {
                                var at_index = en_line.IndexOf("@", at_index_next);
                                Variables.JapaneseAtIndex[Variables.JapaneseLineCounter][i-1] = at_index;
                                at_index_next = at_index + 1;
                            }
                            //See if this line is the end to a Japanese Block
                            if (Variables.EnglishFile[Variables.LinePosition].Contains<char>('\\'))
                            {
                                //The block didn't synced in the previous block end (correspondence error)
                                if (Variables.JapaneseBlockEnd != 0)
                                    correspondenceError();

                                Variables.JapaneseBlockEnd = Variables.LinePosition;
                            }
                        }
                    }
                    //If it appears to be a text line in English
                    else if (en_line.Contains('`'))
                    {
                        Console.WriteLine("English Line at " + (Variables.LinePosition + 1).ToString());
                        //Commenting the English line
                        Variables.EnglishFile[Variables.LinePosition] = Variables.EnglishFile[Variables.LinePosition].Insert(0, ";");

                        //See if this is the start to a new English Block
                        if (Variables.EnglishBlockStart == 0)
                        {
                            Variables.EnglishBlockStart = Variables.LinePosition;
                            Variables.EnglishLineCounter = 0;
                            Variables.EnglishLineIndex = new int[200];
                            Variables.EnglishAtIndex = new int[200][];
                            Variables.EnglishLineIndex[Variables.EnglishLineCounter] = Variables.LinePosition;
                            Variables.EnglishAtIndex[Variables.EnglishLineCounter] = new int[200];
                        }
                        else
                        {
                            Variables.EnglishLineCounter++;
                            Variables.EnglishLineIndex[Variables.EnglishLineCounter] = Variables.LinePosition;
                            Variables.EnglishAtIndex[Variables.EnglishLineCounter] = new int[200];
                        }

                        //Count the number of @ in the text
                        Variables.EnglishATCount += Variables.EnglishFile[Variables.LinePosition].Count(f => f == '@');
                        var at_index_next = 0;
                        for (int i = 1; i <= en_line.Count(f => f == '@'); i++)
                        {
                            var at_index = en_line.IndexOf("@", at_index_next);
                            Variables.EnglishAtIndex[Variables.EnglishLineCounter][i-1] = at_index+1;
                            at_index_next = at_index + 1;
                        }
                        //See if this line is the end to an English Block
                        if (Variables.EnglishFile[Variables.LinePosition].Contains<char>('\\') && (
                            !Variables.EnglishFile[Variables.LinePosition].Contains(".ogg") || 
                            Variables.EnglishFile[Variables.LinePosition].EndsWith("\\")))
                        {
                            Variables.EnglishBlockEnd = Variables.LinePosition;

                            //If the number of @ in English and Japanese is equal, then sync the file
                            //Otherwise, try to assume this isn't a block end, but reather another @
                            if (Variables.EnglishATCount == Variables.JapaneseATCount)
                                tryToSync();
                            else
                                tryToAddExtraAt();
                        }
                    }
                    //If it is a lone uncommented line break
                    else if (en_line.StartsWith("\\"))
                    {
                        if (Variables.JapaneseBlockEnd != 0)
                            correspondenceError();
                        Variables.JapaneseBlockEnd = Variables.LinePosition;
                        Variables.JapaneseLineCounter++;
                        Variables.JapaneseLineIndex[Variables.JapaneseLineCounter] = Variables.LinePosition;

                        Variables.EnglishBlockEnd = Variables.LinePosition;
                        Variables.EnglishLineCounter++;
                        Variables.EnglishLineIndex[Variables.EnglishLineCounter] = Variables.LinePosition;
                        if (Variables.EnglishATCount == Variables.JapaneseATCount)
                            tryToSync();
                        else
                            tryToAddExtraAt();
                    }
                }

                Variables.LinePosition++;
            }

            Console.WriteLine("Writing file...");
            File.WriteAllLines("new_result.txt", Variables.EnglishFile, System.Text.Encoding.GetEncoding("shift_jis"));
            Console.WriteLine("Finished :D\n");
            Console.WriteLine("Results: ");
            Console.WriteLine("\\ without correspondence Error count : " + Variables.SecondEndErrors.ToString());
            Console.WriteLine("\\ Exception Error count : " + Variables.BlockErrors.ToString());
            Console.WriteLine("Insufficient @ Error count : " + Variables.InsufficientAtErrors.ToString());
            Console.WriteLine("@ count Error count : " + Variables.ATCountErrors.ToString());
            Console.WriteLine("Total Error count : " + (Variables.SecondEndErrors + Variables.BlockErrors + Variables.InsufficientAtErrors + Variables.ATCountErrors).ToString());
            Console.ReadKey();
        }

        private static void tryToAddExtraAt()
        {
            if (Variables.EnglishATCount < Variables.JapaneseATCount)
            {
                Variables.InsufficientAtErrors++;
                Console.WriteLine("Insuficient english @ at " + (Variables.LinePosition + 1).ToString());
                Console.WriteLine("English @ count : " + Variables.EnglishATCount.ToString());
                Console.WriteLine("Japanese @ count : " + Variables.JapaneseATCount.ToString());
               // Console.ReadKey();

                Console.WriteLine("Transforming \\ to @ at " + (Variables.LinePosition + 1).ToString());
                Variables.EnglishAtIndex[Variables.EnglishLineCounter][Variables.EnglishFile[Variables.LinePosition].Count(f => f == '@')] = Variables.EnglishFile[Variables.LinePosition].IndexOf('\\');
                Variables.EnglishFile[Variables.LinePosition] = Variables.EnglishFile[Variables.LinePosition].Replace('\\', '@');
                Variables.EnglishATCount++;
                
                if (Variables.EnglishATCount == Variables.JapaneseATCount)
                    tryToSync();
                else
                    Variables.EnglishBlockEnd = 0;
            }
            else
            {
                atCountError();
                tryToSync();
            }
        }

        private static void tryToSync()
        {
            if (Variables.JapaneseBlockEnd != 0)
                sync();
            else
                blockError();
        }

        private static void sync()
        {
            Console.WriteLine("Line Synced at " + (Variables.LinePosition + 1).ToString());
            int dwave_count = 0;
            List<string> dwave_list = new List<string>();
            var dwave_index_array = new int[100][];
            var dwave_line_index = new int[100];
            var dwave_line_counter = 0;
            for (var pos = Variables.EnglishBlockStart; pos <= Variables.EnglishBlockEnd; pos++)
            {
                if (Variables.EnglishFile[pos].Contains("dwave") && Variables.EnglishFile[pos].Contains('`'))
                {
                    dwave_line_index[dwave_line_counter] = pos;
                    dwave_index_array[dwave_line_counter] = new int[100];
                    dwave_count += TextTool.CountStringOccurrences(Variables.EnglishFile[pos], "dwave");
                    var dwave_index_next = 0;
                    for (int i = 1; i <= TextTool.CountStringOccurrences(Variables.EnglishFile[pos], "dwave"); i++)
                    {
                        var dwave_index = Variables.EnglishFile[pos].IndexOf("dwave", dwave_index_next);

                        if (i == 1 && (Variables.EnglishFile[pos].StartsWith(";delay") || Variables.EnglishFile[pos].StartsWith(";wait")))
                            dwave_index_array[dwave_line_counter][i - 1] = 1;
                        else
                            dwave_index_array[dwave_line_counter][i - 1] = dwave_index;

                        var dwave_index_end = dwave_index;
                        while (!Variables.EnglishFile[pos][dwave_index_end].Equals(':') && !Variables.EnglishFile[pos][dwave_index_end].Equals('`') && !Variables.EnglishFile[pos][dwave_index_end].Equals('('))
                        {
                            dwave_index_end++;
                        }
                        if (i == 1 && (Variables.EnglishFile[pos].StartsWith(";delay") || Variables.EnglishFile[pos].StartsWith(";wait")))
                            dwave_list.Add(Variables.EnglishFile[pos].Substring(1, dwave_index_end - 1));
                        else
                            dwave_list.Add(Variables.EnglishFile[pos].Substring(dwave_index, dwave_index_end - dwave_index));
                        
                        dwave_index_next = dwave_index_end;
                    }
                    dwave_line_counter++;
                }
            }
            var counter = (dwave_count - 1);
            int j;
            for (int i = (dwave_line_counter - 1); i >= 0; i--)
            {
                for (j = 0; dwave_line_index[i] != Variables.EnglishLineIndex[j]; j++)
                {
                    /*Console.WriteLine("j == " + j.ToString());
                    Console.WriteLine(dwave_line_index[i].ToString() + Variables.EnglishLineIndex[j].ToString());*/
                }

                for (int k = 99; k >= 0; k--)
                {
                    if (dwave_index_array[i][k] != 0)
                    {
                        if (dwave_index_array[i][k] != 1)
                        {
                            for (int l = 99; l >= 0; l--)
                            {
                                if ((dwave_index_array[i][k] - 1 > 0 && Variables.EnglishAtIndex[j][l] == dwave_index_array[i][k] - 1) ||
                                    (dwave_index_array[i][k] - 2 > 0 && Variables.EnglishAtIndex[j][l] == dwave_index_array[i][k] - 2))
                                {
                                    Variables.EnglishFile[Variables.JapaneseLineIndex[j]] = Variables.EnglishFile[Variables.JapaneseLineIndex[j]].Insert(Variables.JapaneseAtIndex[j][l], ":" + dwave_list[counter].ToString() + ":");
                                }
                            }
                        }
                        else
                        {
                            if(Variables.EnglishFile[Variables.JapaneseLineIndex[j]].StartsWith("!d") || Variables.EnglishFile[Variables.JapaneseLineIndex[j]].StartsWith("!w"))
                            {
                                var delay_index_end = 0;
                                while (Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('!') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('d') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('w') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('0') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('1') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('2') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('3') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('4') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('5') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('6') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('7') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('8') ||
                                       Variables.EnglishFile[Variables.JapaneseLineIndex[j]][delay_index_end].Equals('9'))
                                {
                                    delay_index_end++;
                                }
                                Variables.EnglishFile[Variables.JapaneseLineIndex[j]] = Variables.EnglishFile[Variables.JapaneseLineIndex[j]].Remove(0, delay_index_end);
                            }
                            if (Variables.JapaneseLineIndex[j] == 0)
                            {
                                Console.WriteLine("Something went wrong");
                            }
                            Variables.EnglishFile[Variables.JapaneseLineIndex[j]] = dwave_list[counter].ToString() + ":" + Variables.EnglishFile[Variables.JapaneseLineIndex[j]];
                        }
                        counter--;
                    }
                }
            }
            /*foreach (string dwave in dwave_list)
            {
                Console.WriteLine("dwave : " + dwave.ToString());
            }*/
            Console.WriteLine("dwave count : " + dwave_count.ToString());
            Console.WriteLine("English @ count : " + Variables.EnglishATCount.ToString());
            Console.WriteLine("Japanese @ count : " + Variables.JapaneseATCount.ToString());
            /*if(dwave_count > 0)
                Console.ReadKey();*/
            Variables.EnglishBlockStart = 0;
            Variables.EnglishBlockEnd = 0;
            Variables.EnglishATCount = 0;
            Variables.JapaneseBlockStart = 0;
            Variables.JapaneseBlockEnd = 0;
            Variables.JapaneseATCount = 0;
        }

        private static void blockError()
        {
            Variables.BlockErrors++;
            Console.WriteLine("\\ error at " + (Variables.LinePosition + 1).ToString());
            Console.ReadKey();
        }

        private static void atCountError()
        {
            Variables.ATCountErrors++;
            Console.WriteLine("@ count error at " + (Variables.LinePosition + 1).ToString());
            Console.WriteLine("English @ count : " + Variables.EnglishATCount.ToString());
            Console.WriteLine("Japanese @ count : " + Variables.JapaneseATCount.ToString());
            Console.ReadKey();
        }

        private static void correspondenceError()
        {
            Variables.SecondEndErrors++;
            Console.WriteLine("\\ without correspondence at " + (Variables.LinePosition + 1).ToString());
            Console.ReadKey();
        }
    }
}
