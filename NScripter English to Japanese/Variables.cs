﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NScripter_English_to_Japanese
{
    public class Variables
    {
        private static string[] english_file;
        private static string[] japanese_file;
        private static int[][] en_at_index;
        private static int[][] jp_at_index;
        private static int en_line_counter;
        private static int jp_line_counter;
        private static int[] en_line_index;
        private static int[] jp_line_index;

        public static string[] EnglishFile
        {
            get { return Variables.english_file; }
            set { Variables.english_file = value; }
        }
        public static string[] JapaneseFile
        {
            get { return Variables.japanese_file; }
            set { Variables.japanese_file = value; }
        }

        public static int[][] EnglishAtIndex
        {
            get { return Variables.en_at_index; }
            set { Variables.en_at_index = value; }
        }

        public static int[][] JapaneseAtIndex
        {
            get { return Variables.jp_at_index; }
            set { Variables.jp_at_index = value; }
        }

        public static int EnglishLineCounter
        {
            get { return Variables.en_line_counter; }
            set { Variables.en_line_counter = value; }
        }

        public static int JapaneseLineCounter
        {
            get { return Variables.jp_line_counter; }
            set { Variables.jp_line_counter = value; }
        }

        public static int[] EnglishLineIndex
        {
            get { return Variables.en_line_index; }
            set { Variables.en_line_index = value; }
        }

        public static int[] JapaneseLineIndex
        {
            get { return Variables.jp_line_index; }
            set { Variables.jp_line_index = value; }
        }

        private static int en_start = 0;
        private static int en_end = 0;
        private static int en_at_count = 0;
        private static int jp_start = 0;
        private static int jp_end = 0;
        private static int jp_at_count = 0;
        private static int liner = 0;

        public static int EnglishBlockStart
        {
            get { return Variables.en_start; }
            set { Variables.en_start = value; }
        }
        public static int EnglishBlockEnd
        {
            get { return Variables.en_end; }
            set { Variables.en_end = value; }
        }
        public static int EnglishATCount
        {
            get { return Variables.en_at_count; }
            set { Variables.en_at_count = value; }
        }
        public static int JapaneseBlockStart
        {
            get { return Variables.jp_start; }
            set { Variables.jp_start = value; }
        }
        public static int JapaneseBlockEnd
        {
            get { return Variables.jp_end; }
            set { Variables.jp_end = value; }
        }
        public static int JapaneseATCount
        {
            get { return Variables.jp_at_count; }
            set { Variables.jp_at_count = value; }
        }
        public static int LinePosition
        {
            get { return Variables.liner; }
            set { Variables.liner = value; }
        }

        public static int at_count_errors = 0;
        public static int second_end_errors = 0;
        public static int block_errors = 0;
        public static int insufficient_at_errors = 0;

        public static int ATCountErrors
        {
            get { return Variables.at_count_errors; }
            set { Variables.at_count_errors = value; }
        }
        public static int SecondEndErrors
        {
            get { return Variables.second_end_errors; }
            set { Variables.second_end_errors = value; }
        }
        public static int BlockErrors
        {
            get { return Variables.block_errors; }
            set { Variables.block_errors = value; }
        }
        public static int InsufficientAtErrors
        {
            get { return Variables.insufficient_at_errors; }
            set { Variables.insufficient_at_errors = value; }
        }
    }
}
